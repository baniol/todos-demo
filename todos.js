(function(){

	var todos = function(){

		var self = this;
		var _db;
		var mainContent = document.getElementById('main-content');
		var header = document.getElementById('header-content');

		this.init = function(){
			_db = new Lopers('lopers_demo');
			_db.setTable('categories',['name']);
			_db.setTable('todos',['category','name','done']);

			self.categoryList();

			// main menu event listeners
			document.getElementById('add-todo').addEventListener('click',self.addTodo,false);
			document.getElementById('add-category').addEventListener('click',self.addCategory,false);
			document.getElementById('category-list').addEventListener('click',self.categoryList,false);
		};

		// controllers methods

		this.categoryList = function(){
			mainContent.innerHTML = '';

			// set header text
			header.innerHTML = 'Category list';

			// create category list root node
			var list = document.createElement('ul');
			list.className = 'list';
			list.id = 'cat-list';

			var categories = _db.getRecords('categories');
			var el = '';
			for(var c in categories){
				var allUnits = _db.getRecords('todos','category',categories[c].cid);
				var unitCount = allUnits.length;
				el += '<li data-cid="'+categories[c].cid+'">';
				el += '<span class="name">'+categories[c].name+' ('+unitCount+')</span>';
				el += '<a href="#" class="delete controls">delete</a>';
				el += '<a href="#" class="edit controls">edit</a>';
				el += '</li>';
			}
			list.innerHTML = el;
			mainContent.appendChild(list);

			// add event listeners to each list item
			// edit
			var ed = list.getElementsByClassName('edit');
			for(var i=0;ed.length>i;i++){
				ed[i].addEventListener('click',self.addCategory,false);
			}

			var del = list.getElementsByClassName('delete');
			for(var i=0;del.length>i;i++){
				del[i].addEventListener('click',self.deleteCategory,false);
			}

			var li = list.getElementsByClassName('name');
			for(var i=0;li.length>i;i++){
				li[i].addEventListener('click',self.todoList,false);
			}
		};

		this.addCategory = function(e){
			e.preventDefault();
			mainContent.innerHTML = '';
			// get cid from parent node (li) of the clicked element (.edit)
			var cid = e.target.parentNode.getAttribute('data-cid');
			var name;

			// if edit
			if(cid !== null){
				// @todo - pozbyć się [0]
				header.innerHTML = 'Edit category';
				var el = _db.getRecords('categories','cid',cid)[0];
				name = el.name;
			}else{
				header.innerHTML = 'Add new category';
				name = "";
				cid = '';
			}

			var html = 	'<form action="post" id="save-category" data-cid="'+cid+'" >' +
						'<input type="text" id="cat-name" size="30" value="'+name+'" />' +
						'<input type="submit" value="save" />' +
						'</form>';

			mainContent.innerHTML = html;
			document.getElementById('cat-name').focus();

			// event listener on save button
			document.querySelector('#save-category input[type=submit]')
				.addEventListener('click',self.saveCategory,false);
		};

		this.deleteCategory = function(e){
			e.preventDefault();
			var cid = e.target.parentNode.getAttribute('data-cid');
			if(confirm('Are you sure?')){
				var el = document.querySelector('#cat-list li[data-cid="'+cid+'"]');
				_db.deleteRecord('categories',cid,function(){
					_db.deleteRelated('todos','category',cid);
					document.getElementById('cat-list').removeChild(el);
				});
			}
		};

		this.addTodo = function(e){
			e.preventDefault();
			mainContent.innerHTML = '';
			// get cid from parent node (li) of the clicked element (.edit)
			var cid = e.target.parentNode.getAttribute('data-cid');
			var name,cat;

			// if edit
			if(cid !== null){
				// @todo - pozbyć się [0]
				header.innerHTML = 'Edit item';
				var el = _db.getRecords('todos','cid',cid)[0];
				name = el.name;
				cat = parseInt(el.category);
			}else{
				header.innerHTML = 'Add new item';
				name = "";
				cid = "";
			}

			// select category
			var selectData = _db.getValues('categories','name');
			var options = '';
			for(var i=0;selectData.length>i;i++){
				var selected = selectData[i].key === cat ? 'selected' : null;
				options += '<option value="'+selectData[i].key+'" '+selected+'>'+selectData[i].value+'</option>';
			}

			// html template for adding a new todo item
			var html = 	'<form action="post" id="save-todo" data-cid="'+cid+'">' +
						'<select id="select-cat">'+options+'</select>' + 
						'<textarea type="text" id="todo-name" >' + name + '</textarea>' +
						'<input type="submit" value="save" />' +
						'</form>';

			mainContent.innerHTML = html;
			document.getElementById('todo-name').focus();

			// event listener on save button
			document.querySelector('#save-todo input[type=submit]')
				.addEventListener('click',self.saveTodo,false);
		};

		this.deleteTodo = function(e){
			e.preventDefault();
			var cid = e.target.parentNode.getAttribute('data-cid');
			if(confirm('Are you sure?')){
				var el = document.querySelector('#todo-list li[data-cid="'+cid+'"]');
				_db.deleteRecord('todos',cid,function(){
					document.getElementById('todo-list').removeChild(el);
				});
			}
		};

		this.todoList = function(e){
			var catID;
			if(e.target === undefined){
				catID = e;
			}else{
				catID = e.target.parentNode.getAttribute('data-cid');
			}
			mainContent.innerHTML = '';
			var c = _db.getRecords('categories','cid',catID)[0];
			header.innerHTML = 'Items from category: '+c.name;

			var list = document.createElement('ul');
			list.className = 'list';
			list.id = 'todo-list';

			var todos = _db.getRecords('todos','category',catID);
			var el = '';
			for(var t in todos){
				el += '<li data-cid="'+todos[t].cid+'">';
				el += '<span>'+todos[t].name+'</span>';
				el += '<a href="#" class="delete controls">delete</a>';
				el += '<a href="#" class="edit controls">edit</a>';
				el += '</li>';
			}
			list.innerHTML = el;
			mainContent.appendChild(list);

			// list listeners
			var ed = list.getElementsByClassName('edit');
			for(var i=0;ed.length>i;i++){
				ed[i].addEventListener('click',self.addTodo,false);
			}
			var del = list.getElementsByClassName('delete');
			for(var i=0;del.length>i;i++){
				del[i].addEventListener('click',self.deleteTodo,false);
			}
		};

		// end of controller methods

		this.saveCategory = function(e){
			e.preventDefault();
			var cid = e.target.parentNode.getAttribute('data-cid');
			// @todo js trim
			var catName = document.getElementById('cat-name').value;

			if(catName == ""){
				alert('Field cannot be empty!');
				return false;
			}

			if(cid !== ''){
				_db.editRecord('categories',[catName],cid,function(){
					self.categoryList();
				});
			}else{
				_db.insertRecord('categories',[catName],function(){
					self.categoryList();
				});
			}
		};

		this.saveTodo = function(e){
			e.preventDefault();
			var cid = e.target.parentNode.getAttribute('data-cid');

			// @todo js trim
			var todoName = document.getElementById('todo-name').value;

			if(todoName == ""){
				alert('Field cannot be empty!');
				return false;
			}
			var cat = document.querySelector('#select-cat').value;

			if(cid !== ''){
				_db.editRecord('todos',[cat,todoName,0],cid,function(){
					self.todoList(cat);
				});
			}else{
				_db.insertRecord('todos',[cat,todoName,0],function(){
					self.todoList(cat);
				});
			}
		};
	};

	// try{
		var app = new todos;
		app.init();
	// }
	// catch(e){
	// 	var myStackTrace = e.stack || e.stacktrace || "";
	// 	// throw e;
	// 	console.log(myStackTrace);
	// }
	

	function customError(description,page,line){
		console.log(description);
		console.log(page);
		console.log(line);
		return true;
	}

})();