var Todos = (function(){

	var mainContent = document.getElementById('main-content');
	var header = document.getElementById('header-content');

	var init = function(){
		_db = new Lopers();
		_db.setTable('categories',['name']);
		_db.setTable('todos',['category','name','done']);

		categoryList();

		// main menu event listeners
		document.getElementById('add-todo').addEventListener('click',self.addTodo,false);
		document.getElementById('add-category').addEventListener('click',addCategory,false);
		document.getElementById('category-list').addEventListener('click',self.categoryList,false);
	};

	

	var categoryList = function(){
		mainContent.innerHTML = '';

		// set header text
		header.innerHTML = 'Category list';

		// create category list root node
		var list = document.createElement('ul');
		list.className = 'list';
		list.id = 'cat-list';

		var categories = _db.getRecords('categories');
		var el = '';
		for(var c in categories){
			var allUnits = _db.getRecords('todos','category',categories[c].cid);
			var unitCount = allUnits.length;
			el += '<li data-cid="'+categories[c].cid+'">';
			el += '<span class="name">'+categories[c].name+' ('+unitCount+')</span>';
			el += '<a href="#" class="delete controls">delete</a>';
			el += '<a href="#" class="edit controls">edit</a>';
			el += '</li>';
		}
		list.innerHTML = el;
		mainContent.appendChild(list);

		// add event listeners to each list item
		// edit
		var ed = list.getElementsByClassName('edit');
		for(var i=0;ed.length>i;i++){
			ed[i].addEventListener('click',self.addCategory,false);
		}

		var del = list.getElementsByClassName('delete');
		for(var i=0;del.length>i;i++){
			del[i].addEventListener('click',self.deleteCategory,false);
		}

		var li = list.getElementsByClassName('name');
		for(var i=0;li.length>i;i++){
			li[i].addEventListener('click',self.todoList,false);
		}
	};

	var addCategory = function(e){
		e.preventDefault();
		mainContent.innerHTML = '';
		// get cid from parent node (li) of the clicked element (.edit)
		var cid = e.target.parentNode.getAttribute('data-cid');
		var name;

		// if edit
		if(cid !== null){
			// @todo - pozbyć się [0]
			header.innerHTML = 'Edit category';
			var el = _db.getRecords('categories','cid',cid)[0];
			name = el.name;
		}else{
			header.innerHTML = 'Add new category';
			name = "";
			cid = '';
		}

		var html = 	'<form action="post" id="save-category" data-cid="'+cid+'" >' +
					'<input type="text" id="cat-name" size="30" value="'+name+'" />' +
					'<input type="submit" value="save" />' +
					'</form>';

		mainContent.innerHTML = html;
		document.getElementById('cat-name').focus();

		// event listener on save button
		document.querySelector('#save-category input[type=submit]')
			.addEventListener('click',self.saveCategory,false);
	};

	return{
		init:init
	}

}());

Todos.init();